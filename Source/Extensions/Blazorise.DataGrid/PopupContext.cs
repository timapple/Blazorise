﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Components;

namespace Blazorise.DataGrid
{
    public class PopupContext<TItem>
    {
        public TItem Item { get; set; }
        
        public bool PopupVisible { get; set; }

        public DataGridEditState EditState { get; set; }

        public EventCallback Save { get; set; }

        public EventCallback Cancel { get; set; }

        public IEnumerable<BaseDataGridColumn<TItem>> Columns { get; set; }
        public IReadOnlyDictionary<string, CellEditContext> ItemCellValues { get; set; }

        public IReadOnlyDictionary<string, BaseDataGridColumn<TItem>> ColumnByField { get; set; }
        public IReadOnlyDictionary<string, CellEditContext> ItemCellValueByField { get; set; }

        public PopupContext( IEnumerable<BaseDataGridColumn<TItem>> columns, IReadOnlyDictionary<string, CellEditContext> itemCellValues)
        {
            Columns = columns;
            ColumnByField = columns.Where(c => c.Field != null).ToDictionary(c => c.Field);

            ItemCellValues = itemCellValues;
            //ItemCellValueByField = itemCellValues.ToDictionary(kv => columns.Single(c => c.ElementId == kv.Key).Field, kv => kv.Value);
            //ItemCellValueByField = columns.Where(c => c.Field != null).ToDictionary(c => c.Field, c => ItemCellValues[c.ElementId]);
            var fieldsDic = columns.ToDictionary( c => c.ElementId, c => c.Field );
            var pairs = ItemCellValues.Select( kv => KeyValuePair.Create( fieldsDic[kv.Key], kv.Value ) );
            ItemCellValueByField = new Dictionary<string, CellEditContext>( pairs );
        }

        public T GetValueByField<T>( string field )
        {
            //return ItemCellValueByField[field].CellValue;
            if ( ItemCellValueByField.TryGetValue( field, out var cec ) )
                return (T)cec.CellValue;
            else
                return (T)ColumnByField[field].GetValue( Item );
        }

        public void SetValueByField<T>(string field, T value)
        {
            //ItemCellValueByField[field].CellValue = value;
            if ( ItemCellValueByField.TryGetValue( field, out var cec ) )
                cec.CellValue = value;
            else
                ColumnByField[field].SetValue( Item, value );
        }
    }
}
